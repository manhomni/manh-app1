package com.manh.cp.sample.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("COM.MANH.CP.ITEM")
public interface ItemRestClient {
	
	@RequestMapping(method = RequestMethod.GET, value = "/api/itemservice/item/search?query=\"OrganizationID = '{orgId}'\"")
	public List<Object> getItems(@PathVariable("orgId") String orgId);
}
