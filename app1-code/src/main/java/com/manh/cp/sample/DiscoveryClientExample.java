package com.manh.cp.sample;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

@Component
public class DiscoveryClientExample implements CommandLineRunner {

	@Autowired
	private DiscoveryClient discoveryClient;

	@Override
	public void run(String... strings) throws Exception {

		for (ServiceInstance s : discoveryClient.getInstances("item-service")) { 
			System.out.println(ToStringBuilder.reflectionToString(s));
		}

		for (ServiceInstance s : discoveryClient.getInstances("order-service")) { 
			System.out.println(ToStringBuilder.reflectionToString(s));
		}

	}
}
