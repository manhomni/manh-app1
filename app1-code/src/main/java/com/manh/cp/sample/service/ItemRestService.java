package com.manh.cp.sample.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.manh.cp.sample.domain.Item;

@Component
public class ItemRestService {

	private RestTemplate restTemplate = new RestTemplate();
	
	private static String ORG_ID = "Manh Omni";

	public String createItem(Item item) {

		Map<String, Object> entityMap = new HashMap<String, Object>();
		entityMap.put("OrganizationID", ORG_ID);
		entityMap.put("ItemId", item.getItemId());
		entityMap.put("Description", item.getDesc());
		entityMap.put("Size", item.getSize());
		entityMap.put("ColorCode", item.getColor());

		HttpEntity<Map<String, Object>> httpEntity = new HttpEntity<Map<String, Object>>(entityMap);

		ResponseEntity<Map> response = restTemplate.exchange(getBaseURL() + "/api/itemservice/item", HttpMethod.POST, httpEntity, Map.class);
		Map<String, Object> outputMap = response.getBody();
		String addedPK = (String) outputMap.get("PK");
		return addedPK;
	}
	
	
	public List<Item> listItems() {
		List<Item> itemList = new ArrayList<Item>();
		List outputList = restTemplate.getForObject(getBaseURL() + "/api/itemservice/item/search?query=\"OrganizationID = '"+ORG_ID+"'\"", List.class);		
		for(Object item : outputList) {
			Map<String, Object> itemMap = (Map<String, Object>) item;
			itemList.add(getItem(itemMap));
		} 		
		return itemList;
	}
	
	private Item getItem(Map<String, Object> entityObj) {
		Item item = new Item();
		item.setPk((Long)entityObj.get("PK"));
		item.setItemId((String) entityObj.get("ItemId"));
		item.setDesc((String) entityObj.get("Description"));
		item.setColor((String)entityObj.get("ColorCode"));
		item.setSize((String) entityObj.get("Size"));
		return item;
	}

	private String getBaseURL() {
		return "http://localdocker:8081";
	}
}
