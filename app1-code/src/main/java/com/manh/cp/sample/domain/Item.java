package com.manh.cp.sample.domain;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class Item {

	private Long pk;	
	
	@Size(min=2, max=30)
	private String orgId;	
	
	@Size(min=2, max=30)
	
	private String itemId;	
	
	private String desc;	
	
	private String size;	
	
	private String color;	
	
	
	
}
