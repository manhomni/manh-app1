package com.manh.cp.sample.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.manh.cp.sample.domain.Item;
import com.manh.cp.sample.service.ItemRestService;

@Controller
public class ItemCrudController {

	@Autowired
	private ItemRestService itemRestService;
	
	@RequestMapping("/")
	public String index(Model model) {	
		model.addAttribute("items", itemRestService.listItems());
		return "index";
	}
	
	@RequestMapping(value = "/create-item", method = RequestMethod.GET)
	public String createItemForm(Item item) {
		return "create-item";
	}
	
	@RequestMapping(value="/create-item", method=RequestMethod.POST)
    public String createItemSubmit(@Valid Item item, BindingResult bindingResult) {        
		if (bindingResult.hasErrors()) {
        	return "create-item";
        }		
		itemRestService.createItem(item);
        return "redirect:/";
    }
}
