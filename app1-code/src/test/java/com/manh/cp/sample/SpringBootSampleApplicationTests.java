package com.manh.cp.sample;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.Test;

@SpringApplicationConfiguration(classes = SpringBootSampleApplication.class)
@WebAppConfiguration
@ActiveProfiles("scratch")
public class SpringBootSampleApplicationTests extends AbstractTestNGSpringContextTests {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;
	
//	@BeforeTest
//	public void setUp() {
//		this.mvc = MockMvcBuilders.
//						webAppContextSetup(this.context)
//						.apply(springSecurity())
//						.build();
//	}

	@Test
	public void testHome() throws Exception {
		this.mvc = MockMvcBuilders.
				webAppContextSetup(this.context)
				.apply(springSecurity())
				.build();
		this.mvc.perform(get("/api/hello").with(user("user"))).andExpect(status().isOk())
				.andExpect(content().string("Bath"));
	}

}
