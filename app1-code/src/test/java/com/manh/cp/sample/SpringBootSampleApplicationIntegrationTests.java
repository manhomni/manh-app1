package com.manh.cp.sample;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@SpringApplicationConfiguration(classes = SpringBootSampleApplication.class)
@WebIntegrationTest("server.port:0")
@ActiveProfiles("scratch")
@DirtiesContext
public class SpringBootSampleApplicationIntegrationTests extends AbstractTestNGSpringContextTests {

	@Value("${local.server.port}")
	private int port;

	@Test
	public void testHome() throws Exception {
		System.out.println("################################");
		System.out.println("################################");
		System.out.println("################################");
		System.out.println("################################");
		System.out.println("################################");
		System.out.println("################################");
		System.out.println("################################");
		System.out.println(port);
	}

}
