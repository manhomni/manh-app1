# Cloud Platform Skeleton Application

This is a SpringBoot project based on springboot-eclipselink sample app available here  https://github.com/dsyer/spring-boot-sample-data-eclipselink.
The github project uses maven whereas this one uses gradle as its build tool. In adding to this, it  has the following things integrated in it
  
  - Integrates JPA-2.1 and uses EclipseLink as it implementation.
    - Does dynamic weaving for all the domain objects. This needs Spring Instrumentation project to be integrated.
    - Spring Instrument needs to be added as a javaagent for test case execution and running the application. This is taken care by gradle script.
    - Does all required wiring for getting spring data, jdbc to all work where jpa
  - Integrates Flyway for database migrations.
    - All DB incremental script are part of the source code repo.
    - DB gets auto upgraded based on the application build.
  - Integrates QueryDSL
    - QueryDSL does compile time code generation for all entities.
    - Gradle script handles this seamlessly.

## Setting up Eclipse Project
Clone the repo and import the project as a Gradle Project. Gitcast to come soon...
After import,  manually refresh the project's source folders info from menu "Gradle >> Refresh Source Folders".

## Running the application
Run eclipse, right click the app and run it as spring boot application.
If you want to run from gradle, run gradle run
