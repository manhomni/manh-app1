#!/bin/sh
echo creating archive of local src code....
rm -f latest-src.tar.gz
uploadStash=`git stash create`; git archive ${uploadStash:-HEAD} | gzip -n > "latest-src.tar.gz"
echo creating remote directory stucture....
docker-machine ssh $1 "rm -rf $(pwd)"
docker-machine ssh $1 "mkdir -p $(pwd)"
docker-machine scp latest-src.tar.gz $1:/$(pwd)
docker-machine ssh $1 "cd $(pwd) && tar -xvf latest-src.tar.gz"