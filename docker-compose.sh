#!/bin/bash
echo "Running docker-compose file: $COMPOSE_FILE"
docker run -v "/$(pwd)":/app \
           -v "/$(pwd):$(pwd)" \
           -v //var/run/docker.sock:/var/run/docker.sock \
           -e COMPOSE_PROJECT_NAME=$(basename "/$(pwd)")\
           -e COMPOSE_FILE=$COMPOSE_FILE \
           -w "/$(pwd)" \
           -ti --rm \
           dduportal/docker-compose:latest ${1+"$@"}